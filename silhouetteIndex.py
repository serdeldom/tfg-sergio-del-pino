from sklearn.cluster import KMeans 
from sklearn.metrics import silhouette_score 
import pandas as pd

# Generating the sample data from make_blobs 
  
path = r'C:\Users\Sergio\Documents\My Electric Avenue Technical Data.tar\Datasets originales'
  
# loading the dataset 

X = pd.read_csv(path + "\\participantDataMinable.csv")
X = X.dropna()
X1 = X.copy()

X = pd.get_dummies(X1)

  
no_of_clusters = [2, 3, 4, 5, 6, 7, 8, 9] 
  
for n_clusters in no_of_clusters: 
  
    cluster = KMeans(n_clusters = n_clusters) 
    cluster_labels = cluster.fit_predict(X) 
  
    # The silhouette_score gives the  
    # average value for all the samples. 
    silhouette_avg = silhouette_score(X, cluster_labels) 
  
    print("For no of clusters =", n_clusters, 
          " The average silhouette_score is :", silhouette_avg) 