import csv

path = r'C:\Users\Sergio\Documents\My Electric Avenue Technical Data.tar\Datasets originales'

MCName = []
P1Current = []
totalP1C = 0
lenP1C = 0
P2Current = []
totalP2C = 0
lenP2C = 0
P3Current = []
totalP3C = 0
lenP3C = 0

aux = ""
i = 0
fileobj = open(path + "\\MCData.csv", "r")

for line in fileobj.readlines():
    rip = str(line.strip()).split(',')

    if i == 0:
        #Aquí es el caso inicial para la cabecera del csv
        MCName.append(str(rip[0]))
        P1Current.append(str(rip[3]))
        P2Current.append(str(rip[7]))
        P3Current.append(str(rip[11]))
            
    elif aux == str(rip[0]) or aux == "":
        #Es el caso en el que no cambie el MC Name, por lo que sigue el mismo clúster
        aux = str(rip[0])

        if str(rip[3]) != "NULL":
            totalP1C += float(rip[3])
            lenP1C += 1

        if str(rip[7]) != "NULL":
            totalP2C += float(rip[7])
            lenP2C += 1

        if str(rip[11]) != "NULL":
            totalP3C += float(rip[11])
            lenP3C += 1

    elif aux != str(rip[0]) or (str(rip[0])=="Your Homes" and str(rip[2])=="2015-10-07 09:59:00.000"):
        #Cuando hay un cambio de MC NAME/Cluster, se pasan a las listas el nombre y las medias de las corrientes, y se reinician las variables.
        MCName.append(aux)
        P1Current.append(round(totalP1C/lenP1C,2))
        P2Current.append(round(totalP2C/lenP2C, 2))
        P3Current.append(round(totalP3C/lenP3C,2))

        aux = str(rip[0])
        if str(rip[3]) != "NULL":
            totalP1C = float(rip[3])
            lenP1C = 1
        else:
            totalP1C = 0
            lenP1C = 0
        
        if str(rip[7]) != "NULL":
            totalP2C = float(rip[7])
            lenP2C = 1
        else:
            totalP2C = 0
            lenP2C = 0

        if str(rip[11]) != "NULL":
            totalP3C = float(rip[11])
            lenP3C = 1
        else:
            totalP3C = 0
            lenP3C = 0

    if str(rip[0])=="Your Homes" and str(rip[2])=="2015-10-07 09:59:00.000":
        #Este es el caso final, al ser la última entrada no cambia el clúster pero sí se acaban los datos de éste.
        MCName.append(aux)
        P1Current.append(round(totalP1C/lenRP1C, 2))
        P2Current.append(round(totalP2C/lenP2C,2))
        P3Current.append(round(totalP3C/lenP3C,2))
        
    i+=1

fileobj.close()

newFile = open(path + "\\prueba.csv", 'w')

data = []
rows = []

for n in range(0, len(MCName)):
    print(n)
    rows.append(MCName[n])
    rows.append(P1Current[n])
    rows.append(P2Current[n])
    rows.append(P3Current[n])

    data.append(rows)
    rows  = []

with newFile:
    writer = csv.writer(newFile)
    writer.writerows(data)