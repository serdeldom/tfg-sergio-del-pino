import pandas as pd
import numpy as np
from scipy import stats
from datetime import datetime
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import r2_score
import matplotlib.pyplot as plt

def percentage_error(actual, predicted):
    res = np.empty(actual.shape)
    for j in range(actual.shape[0]):
        if actual[j] != 0:
            res[j] = (actual[j] - predicted[j]) / actual[j]
        else:
            res[j] = predicted[j] / np.mean(actual)
    return res

def mean_absolute_percentage_error(y_true, y_pred): 
    return np.mean(np.abs(percentage_error(np.asarray(y_true), np.asarray(y_pred)))) * 100
    

path = r'C:\Users\Sergio\Documents\My Electric Avenue Technical Data.tar\Datasets originales'


df = pd.read_csv(path + "\\chargesRegresion.csv", index_col=[0],        date_parser=lambda x: datetime.strptime(x, "%Y-%m-%d %H:%M:%S"))
df.head()


#No hay anomalias aparentemente
#df.plot(figsize=(22,8))

#Parece que hay demasiadas cargas con distancias semanales muy cercanas a 0
#df.hist()

#Esto solo si la regresion lineal sale rara, porque los datos no siguen una distribución normal
#std_dev = 3
#df = df[(np.abs(stats.zscore(df)) < float(std_dev)).all(axis=1)]
#df.plot(figsize=(18,5))

#df.loc['2014-07-08 13:45:00', ['Starting SoC in this charge']].plot()
#plt.show()

#Para ver si de verdad hay linealidad entre las variables, esto debería hacerlo con la variable objetivo
#plt.scatter(df["Days passed since last charge"], df["Date of charge"])
plt.scatter(df["Distance since last charge"], df["Starting SoC in this charge"])
#plt.scatter(df["Starting SoC in this charge"], df["Distance since last charge"])
plt.show()
# X = pd.DataFrame(df[["Days passed since last charge", "Ending SoC of last charge", "Distance since last charge"]])
# y = pd.DataFrame(df["Starting SoC in this charge"]).astype(int)

# model = LinearRegression()
# scores = []

# X_train, X_test, Y_train, Y_test = train_test_split(X, y, test_size=0.3, random_state=1)
# model.fit(X_train, Y_train)
# Y_pred = model.predict(X_test)

# print("The mean absolute error is", mean_absolute_error(Y_test, Y_pred))
# print("The mean absolute percentage error is", mean_absolute_percentage_error(Y_test, Y_pred))
# print("The R^2 score is", r2_score(Y_test, Y_pred))