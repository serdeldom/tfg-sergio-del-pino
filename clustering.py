import pandas as pd
import csv
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.impute import SimpleImputer
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import train_test_split
from sklearn import metrics

path = r'C:\Users\Sergio\Documents\My Electric Avenue Technical Data.tar\Datasets originales'

dataframe = pd.read_csv(path + "\\participantDataMinable.csv", encoding = "ISO-8859-1", skip_blank_lines=True)
#dataframe.dropna()
X = np.array(dataframe[["Distance per week", "Duration of trips", "Charges per week", "Power charged per week", "Duration of charges"]])

train, test = train_test_split(X, test_size=0.2)

kmeans = KMeans(n_clusters=3).fit(X)
centroids = kmeans.cluster_centers_

y = kmeans.labels_


plt.scatter(X[:,0], X[:,1], c= kmeans.labels_, s=50, alpha=0.5)
plt.scatter(centroids[:, 0], centroids[:, 1], c='red', s=50)
plt.show()

with open(path + '\\participantDataMinableClusters.csv', 'w', ) as myfile:
    wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
    wr.writerow(["Label"])
    for word in y:
        wr.writerow([word])