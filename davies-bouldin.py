from sklearn.cluster import KMeans 
from sklearn.metrics import davies_bouldin_score 
import pandas as pd
import numpy as np
from sklearn.impute import SimpleImputer

path = r'C:\Users\Sergio\Documents\My Electric Avenue Technical Data.tar\Datasets originales'
  
# loading the dataset 

dataframe = pd.read_csv(path + "\\participantDataMinable.csv", encoding = "ISO-8859-1", skip_blank_lines=True)
#dataframe.dropna()
X = np.array(dataframe[["Distance per week", "Duration of trips", "Charges per week", "Power charged per week", "Duration of charges"]])
print(X.shape)
  
# K-Means
for n in range(2, 10):
    kmeans = KMeans(n_clusters=n, random_state=10).fit(X) 
    # we store the cluster labels 
    labels = kmeans.labels_ 
    print("The DB score for " + str(n) + " clusters is " + str(davies_bouldin_score(X, labels))) 