
kf = StratifiedKFold(n_splits=3)

kmeansTrain = KMeans(n_clusters=3).fit(train)
y = kmeansTrain.labels_

scores = cross_val_score(kmeansTrain, train, y, cv=kf, scoring="accuracy")

print("Media de cross_validation", scores.mean())
 
preds = kmeansTrain.predict(test)
 
score_pred = metrics.accuracy_score(preds, preds)
 
print("Metrica en Test", score_pred)