import tensorflow as tf
import tensorflow.keras as keras
from tensorflow.keras.datasets import mnist
from tensorflow.keras.layers import Input, Dense, Dropout, Flatten, Conv2D, MaxPool2D
from tensorflow.keras import backend as K
import numpy as np
import matplotlib.pyplot as plt

print(np.random.seed(1))
print(tf.random.set_seed(1))