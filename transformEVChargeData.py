import csv
from datetime import datetime

path = r'C:\Users\Sergio\Documents\My Electric Avenue Technical Data.tar\Datasets originales'

ParticipantID= []
charges = []
powerRec = []
power = 0
weeks = 0
chargesPerPart = 0
durationCharges = []
durationCh = 0
durationTrips = []
durationTr = 0
distances = []
dist = 0
trips = 0

aux = ""
i = 0

fileobj = open(path + "\\EVChargeData.csv", "r")
fileobj1 = open(path + "\\EVTripData.csv", "r")

for line in fileobj.readlines():
    rip = str(line.strip()).split(';')

    if i == 0:
        #Aquí es el caso inicial para la cabecera del csv
        ParticipantID.append(str(rip[0]))
        charges.append("Charges per week")
        powerRec.append("Power charged per week")
        durationCharges.append("Duration of charges")
        
    elif aux == "":
        #Aquí cojo el primer participante y la primera carga de todas
        aux = str(rip[0])
        startChDate = str(rip[1])
        startDate = datetime.strptime(startChDate, '%d/%m/%Y %H:%M')
        date = startDate.strftime('%V')
        stopChDate = str(rip[2])
        stopDate = datetime.strptime(stopChDate, '%d/%m/%Y %H:%M')
        chargesPerPart +=1
        power += (int(rip[4])-int(rip[3]))
        durationCh += (stopDate - startDate).seconds
        #(stopDate - startDate).seconds/3600 Así puedo conseguir la duración en horas
        #print(60*0.6833) Así puedo conseguir los minutos exactos (haciendo round() )
            
    elif aux == str(rip[0]):
        #Es el caso en el que no cambie el ParticipantID
        startChDate = str(rip[1])
        startDate = datetime.strptime(startChDate, '%d/%m/%Y %H:%M') 
        stopChDate = str(rip[2])
        stopDate = datetime.strptime(stopChDate, '%d/%m/%Y %H:%M')
        durationCh += (stopDate - startDate).seconds
        chargesPerPart += 1

        if date == datetime.strptime(str(rip[1]), '%d/%m/%Y %H:%M').strftime('%V'):
            #Ha habido una nueva recarga del participante en la semana
            power += (int(rip[4])-int(rip[3]))
        else:
            #Ha cambiado de semana, por lo que 
            weeks += 1
            power += (int(rip[4])-int(rip[3]))
            date = startDate.strftime('%V')

    elif aux != str(rip[0]) and aux != "":
        #Cuando hay un cambio de ParticipantID, se pasan a las listas el ParticipantID y las medias de recargas por semana, y se reinician las variables.
        ParticipantID.append(aux)
        med = round(chargesPerPart / weeks, 2)
        charges.append(med)
        recharge = round(power/weeks, 2)
        powerRec.append(recharge)
        duration = round(durationCh / chargesPerPart, 2)
        durationCharges.append(duration)

        aux = str(rip[0])
        chargesPerPart = 1
        power=1
        weeks = 1
        startChDate = str(rip[1])
        startDate = datetime.strptime(startChDate, '%d/%m/%Y %H:%M')
        date = startDate.strftime('%V')
        stopChDate = str(rip[2])
        stopDate = datetime.strptime(stopChDate, '%d/%m/%Y %H:%M')
        durationCh = (stopDate - startDate).seconds

       
    if i==76698:
        #Este es el caso final, al ser la última entrada no cambia el clúster pero sí se acaban los datos de éste.
        chargesPerPart += 1
        power += (int(rip[4])-int(rip[3]))
        ParticipantID.append(aux)
        med = round(chargesPerPart / weeks, 2)
        charges.append(med)
        recharge = round(power/weeks, 2)
        powerRec.append(recharge)

        startChDate = str(rip[1])
        startDate = datetime.strptime(startChDate, '%d/%m/%Y %H:%M')
        date = startDate.strftime('%V')
        stopChDate = str(rip[2])
        stopDate = datetime.strptime(stopChDate, '%d/%m/%Y %H:%M')
        durationCh += (stopDate - startDate).seconds
        durationCh = durationCh/3600 #Paso a minutos
        duration = round(durationCh / chargesPerPart,2)
        durationCharges.append(duration)


    i+=1

fileobj.close()

i=0
aux = ""
for line in fileobj1.readlines():
    rip = str(line.strip()).split(';')


    if i == 0:
        #Aquí es el caso inicial para la cabecera del csv
        distances.append("Distance per week")
        durationTrips.append("Duration of trips")
        
    elif aux == "":
        #Aquí cojo el primer participante y la primera carga de todas
        aux = str(rip[0])
        startTrDate = str(rip[1])
        startDate = datetime.strptime(startTrDate, '%d/%m/%Y %H:%M')
        date = startDate.strftime('%V')
        stopTrDate = str(rip[2])
        stopDate = datetime.strptime(stopTrDate, '%d/%m/%Y %H:%M')
        durationTr += (stopDate - startDate).seconds
        dist += int(rip[3])
        trips += 1
            
    elif aux == str(rip[0]):
        #Es el caso en el que no cambie el ParticipantID

        startTrDate = str(rip[1])
        startDate = datetime.strptime(startTrDate, '%d/%m/%Y %H:%M')
        stopTrDate = str(rip[2])
        stopDate = datetime.strptime(stopTrDate, '%d/%m/%Y %H:%M')
        durationTr += (stopDate - startDate).seconds
        trips += 1

        if date == datetime.strptime(str(rip[1]), '%d/%m/%Y %H:%M').strftime('%V'):
            #Ha habido una nueva recarga del participante en la semana
            dist +=int(rip[3])

        else:
            #Ha cambiado de semana, por lo que 
            weeks += 1
            dist +=int(rip[3])
            date = startDate.strftime('%V')


    elif aux != str(rip[0]) and aux != "":
        #Cuando hay un cambio de ParticipantID, se pasan a las listas el ParticipantID y las medias de recargas por semana, y se reinician las variables.
        dist = dist/1000 #Paso a kilometros las distancia total
        total = round(dist/weeks, 2)
        distances.append(total)
        durationTr = durationTr/3600 #Paso la duración de segundos a horas
        duration = round(durationTr/trips,2)
        durationTrips.append(duration)

        startTrDate = str(rip[1])
        startDate = datetime.strptime(startTrDate, '%d/%m/%Y %H:%M')
        stopTrDate = str(rip[2])
        stopDate = datetime.strptime(stopTrDate, '%d/%m/%Y %H:%M')
        durationTr += (stopDate - startDate).seconds

        aux = str(rip[0])
        dist = 1
        weeks = 1
        trips = 1

       
    if i==383051:
        #Este es el caso final, no cambia el cluster, se acaban sus datos
        dist +=int(rip[3])
        #Pasar a kilometros en vez de metros semanales
        dist = dist/1000
        total = round(dist/weeks, 2)
        distances.append(total)
        startTrDate = str(rip[1])
        startDate = datetime.strptime(startTrDate, '%d/%m/%Y %H:%M')
        stopTrDate = str(rip[2])
        stopDate = datetime.strptime(stopTrDate, '%d/%m/%Y %H:%M')
        durationTr += (stopDate - startDate).seconds
        durationTr = durationTr/3600 #Paso la duración de segundos a horas
        duration = round(durationTr/trips, 2)
        durationTrips.append(duration)
    i+=1

fileobj1.close()

newFile = open(path + "\\participantDataMinable.csv", 'w')

data = []
rows = []

for n in range(0, len(ParticipantID)):
    rows.append(ParticipantID[n])
    rows.append(distances[n])
    rows.append(durationTrips[n])
    rows.append(charges[n])
    rows.append(durationCharges[n])
    rows.append(powerRec[n])

    data.append(rows)
    rows  = []

with newFile:
    writer = csv.writer(newFile)
    writer.writerows(data)