import pandas as pd
import graphviz 
import os
os.environ["PATH"] += os.pathsep + 'C:\Program Files (x86)\Graphviz2.38\bin'
import csv
import numpy as np
from sklearn.cluster import KMeans 
from sklearn import tree
from sklearn.impute import SimpleImputer
from sklearn.model_selection import train_test_split


path = r'C:\Users\Sergio\Documents\My Electric Avenue Technical Data.tar\Datasets originales'

X = pd.read_csv(path + "\\participantDataMinable.csv", encoding = "ISO-8859-1", skip_blank_lines=True)
X = X.values[:, 1:6]
Y = pd.read_csv(path + '\\participantDataMinableClusters.csv', encoding = "ISO-8859-1", skip_blank_lines=True)
Y = Y.values[:, 0]



clf = tree.DecisionTreeClassifier()

clf = clf.fit(X, Y)


dot_data = tree.export_graphviz(clf, out_file=None, 
                      feature_names=["Distance per week", "Duration of trips", "Charges per week",  "Duration of charges" ,"Power charged per week"],  
                      class_names= ["0", "1", "2"],  
                      filled=True, rounded=True,  
                      special_characters=True) 
graph = graphviz.Source(dot_data, format='png') 
graph.render("decisionTreePD") 
