import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.preprocessing import MinMaxScaler
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix


path = r'C:\Users\Sergio\Documents\My Electric Avenue Technical Data.tar\Datasets originales'


X = pd.read_csv(path + "\\participantDataMinable.csv", encoding = "ISO-8859-1", skip_blank_lines=True)
X = X.values[:, 1:6]
Y = pd.read_csv(path + '\\participantDataMinableClusters.csv', encoding = "ISO-8859-1", skip_blank_lines=True)
y = Y.values[:, 0]


 
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.35, random_state=1)


# Para encontrar el número de vecinos óptimo
""" for n_neighbors in range(1, 11):
     print(n_neighbors)
     knn = KNeighborsClassifier(n_neighbors)
     knn.fit(X_train, y_train)
     print('Accuracy of K-NN classifier on training set: {:.2f}'
          .format(knn.score(X_train, y_train)))
     print('Accuracy of K-NN classifier on test set: {:.2f}'
          .format(knn.score(X_test, y_test)))

     scores = cross_val_score(knn, X, y, cv=10, scoring='accuracy')
     print(scores) """

n_neighbors = 5

knn = KNeighborsClassifier(n_neighbors)
knn.fit(X_train, y_train)
print('Accuracy of K-NN classifier on training set: {:.2f}'
     .format(knn.score(X_train, y_train)))
print('Accuracy of K-NN classifier on test set: {:.2f}'
     .format(knn.score(X_test, y_test)))

scores = cross_val_score(knn, X, y, cv=10, scoring='accuracy')

# Predicción de nuevos datos
Z = pd.read_csv(path + "\\MOCK_DATA.csv",  encoding = "ISO-8859-1", skip_blank_lines=True, delimiter=";")
Z = Z.values[:, 0:6]


predicted = knn.predict(Z)
print(predicted)