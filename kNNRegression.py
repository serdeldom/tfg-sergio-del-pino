import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import neighbors
from datetime import datetime
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_absolute_error, r2_score
import matplotlib.pyplot as plt

def percentage_error(actual, predicted):
    res = np.empty(actual.shape)
    for j in range(actual.shape[0]):
        if actual[j] != 0:
            res[j] = (actual[j] - predicted[j]) / actual[j]
        else:
            res[j] = predicted[j] / np.mean(actual)
    return res

def mean_absolute_percentage_error(y_true, y_pred): 
    return np.mean(np.abs(percentage_error(np.asarray(y_true), np.asarray(y_pred)))) * 100
    

path = r'C:\Users\Sergio\Documents\My Electric Avenue Technical Data.tar\Datasets originales'

df = pd.read_csv(path + "\\chargesRegresion.csv", index_col=[0],        date_parser=lambda x: datetime.strptime(x, "%Y-%m-%d %H:%M:%S"))
df.head()


X = pd.DataFrame(df[["Days passed since last charge", "Ending SoC of last charge", "Distance since last charge"]])
y = pd.DataFrame(df["Starting SoC in this charge"]).astype(int)

X_train, X_test, Y_train, Y_test = train_test_split(X, y, test_size=0.3, random_state=123)

# ajuste del 1º modelo de regresion

for n_neighbors in range(1, 14):

    for i, weights in enumerate(['uniform', 'distance']):
        knn = neighbors.KNeighborsRegressor(n_neighbors, weights=weights)
        knn.fit(X_train, Y_train)

        Y_pred = knn.predict(X_test).ravel()

        print("\n", "The scores for", n_neighbors, "neighbors, and weight", weights)
        print("The mean absolute error is", mean_absolute_error(Y_test, Y_pred))
        print("The mean absolute percentage error is", mean_absolute_percentage_error(Y_test, Y_pred))
        print("The R^2 score is", r2_score(Y_test, Y_pred))



