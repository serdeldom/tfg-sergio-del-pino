import csv
from datetime import datetime
import sqlite3

path = r'C:\Users\Sergio\Documents\My Electric Avenue Technical Data.tar\Datasets originales'

datePrev = datetime.now().date()

fileobj = open(path + "\\timelineChargesRegression.csv", "r")
filenew= open(path + "\\chargesRegresion.csv", "w")

i=0

for line in fileobj.readlines():
    rip = str(line.strip()).split(',')

    if i==0:
        filenew.write(line)
    elif str(rip[0])!="":
         if  (datePrev != datetime.strptime(str(rip[0]), '%Y-%m-%d %H:%M:%S').date()) or rip[4]!="0":
            filenew.write(line)
            datePrev = datetime.strptime(str(rip[0]), '%Y-%m-%d %H:%M:%S').date()



    i +=1

fileobj.close()
