import csv

path = r'C:\Users\Sergio\Documents\My Electric Avenue Technical Data.tar\Datasets originales'


labels = []
distances = []
recharges = []
powers = []
counts = []

distanceClust0 = 0
rechargesClust0 = 0
powerClust0 = 0
count0 = 0
distanceClust1 = 0
rechargesClust1 = 0
powerClust1 = 0
count1 = 0
distanceClust2 = 0
rechargesClust2 = 0
powerClust2 = 0
count2= 0
distanceClust3 = 0
rechargesClust3 = 0
powerClust3 = 0
count3 = 0
i = 0



fileobj = open(path + "\\participantDataClustered.csv", "r")

for line in fileobj.readlines():
    rip = str(line.strip()).split(';')

    if i == 0:
        labels.append(str(rip[4]))
        distances.append("Distance per week")
        recharges.append("Recharges per week")
        powers.append("Power recharged per week")
        counts.append("Number of members")

    if str(rip[4])== "0":
        distanceClust0 += float(rip[1])
        rechargesClust0 += float(rip[2])
        powerClust0 += float(rip[3])
        count0 +=1

    if str(rip[4])== "1":
        distanceClust1 += float(rip[1])
        rechargesClust1 += float(rip[2])
        powerClust1 += float(rip[3])
        count1 +=1

    if str(rip[4])== "2":
        distanceClust2 += float(rip[1])
        rechargesClust2 += float(rip[2])
        powerClust2 += float(rip[3])
        count2 +=1

    if str(rip[4])== "3":
        distanceClust3 += float(rip[1])
        rechargesClust3 += float(rip[2])
        powerClust3 += float(rip[3])
        count3 +=1

    if i == 215:
        distanceClust0 = distanceClust0/ count0
        rechargesClust0 = rechargesClust0 /count0
        powerClust0 = powerClust0 / count0
        labels.append(0)
        distances.append(distanceClust0)
        recharges.append(rechargesClust0)
        powers.append(powerClust0)
        counts.append(count0)

        distanceClust1 = distanceClust1/ count1
        rechargesClust1 = rechargesClust1 /count1
        powerClust1 = powerClust1 / count1
        labels.append(1)
        distances.append(distanceClust1)
        recharges.append(rechargesClust1)
        powers.append(powerClust1)
        counts.append(count1)

        distanceClust2 = distanceClust2/ count2
        rechargesClust2 = rechargesClust2 /count2
        powerClust2 = powerClust2 / count2
        labels.append(2)
        distances.append(distanceClust2)
        recharges.append(rechargesClust2)
        powers.append(powerClust2)
        counts.append(count2)

        if count3 > 0:
            distanceClust3 = distanceClust3/ count3
            rechargesClust3 = rechargesClust3 /count3
            powerClust3 = powerClust3 / count3
            labels.append(3)
            distances.append(distanceClust3)
            recharges.append(rechargesClust3)
            powers.append(powerClust3)
            counts.append(count3)
    i += 1

newFile = open(path + "\\clustersParticipantDataK4.csv", 'w')
data = []
rows = []

for n in range(0, len(labels)):
    rows.append(labels[n])
    rows.append(counts[n])
    rows.append(distances[n])
    rows.append(recharges[n])
    rows.append(powers[n])

    data.append(rows)
    rows  = []

with newFile:
    writer = csv.writer(newFile)
    writer.writerows(data)