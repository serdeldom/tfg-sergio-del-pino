import csv
from datetime import datetime
import sqlite3

path = r'C:\Users\Sergio\Documents\My Electric Avenue Technical Data.tar\Datasets originales'

Participants = []
TimestampStartCharges = []
DaysSinceLastCharge = []
days = 0
StartingSoCs = []
starting = 0
EndingSoCs = []
ending = 0
DistanceTillCharge = []
distance = 0

aux = ""
endSocLast = 0
i = 0
fileobj = open(path + "\\EVChargeData.csv", "r")

con = sqlite3.Connection('newdb.sqlite')
cur = con.cursor()
cur.execute("DROP TABLE TRIPDATA")
cur.execute('CREATE TABLE "TRIPDATA" ("id" varchar(12), "dateTrip" timestamp, "dateStop" timestamp, "distance" integer, "power" integer, "odometer" integer); ')
f = open(path + "\\EVTripData.csv", "r")
csv_reader = csv.reader(f, delimiter = ";")
cur.executemany('INSERT INTO TRIPDATA VALUES (?, ?, ?, ?, ?, ?)', csv_reader)

for line in fileobj.readlines():
    rip = str(line.strip()).split('\t')

    if i == 0:
        #Aquí es el caso inicial para la cabecera del csv
        Participants.append(str(rip[0]))
        TimestampStartCharges.append("Date of charge")
        DaysSinceLastCharge.append("Days passed since last charge")
        StartingSoCs.append("Starting SoC in this charge")
        EndingSoCs.append("Ending SoC of last charge")
        DistanceTillCharge.append("Distance since last charge")

    #CAMBIO DE PARTICIPANTE, SERÍA LA PRIMERA CARGA DE ESTE PARTICIPANTE
    elif  aux == "" or aux != str(rip[0]):
        Participants.append(str(rip[0]))
        charge = datetime.strptime(str(rip[1]), '%d/%m/%Y %H:%M')
        TimestampStartCharges.append(charge)
        days=0
        DaysSinceLastCharge.append(days)
        lastCharge = charge
        starting = int(rip[3])
        StartingSoCs.append(starting)
        endSoCLast = 12
        EndingSoCs.append(endSoCLast)
        endSoCLast = int(rip[4])
        aux = str(rip[0])

    #OTRA CARGA DEL MISMO PARTICIPANTE
    elif aux == str(rip[0]):
        Participants.append(str(rip[0]))
        charge = datetime.strptime(str(rip[1]), '%d/%m/%Y %H:%M')
        TimestampStartCharges.append(charge)
        days = (charge - lastCharge).days
        DaysSinceLastCharge.append(days)
        lastCharge = charge
        starting = int(rip[3])
        StartingSoCs.append(starting)
        EndingSoCs.append(endSoCLast)
        endSoCLast = int(rip[4]) 

    if i!=0:
        print(i)
        cur.execute('SELECT dateTrip, distance FROM TRIPDATA WHERE id=?', (aux,))
        trips = cur.fetchall()
        for t in trips:
            startTrip = datetime.strptime(t[0], '%d/%m/%Y %H:%M')
            if (charge>startTrip) and ((charge - startTrip).days < days):
                distance += (t[1]/1000) 
        DistanceTillCharge.append(distance)
        distance = 0

    i+=1

fileobj.close()


cur.close()
con.commit()
con.close()
f.close()


newFile = open(path + "\\timelineChargesRegression.csv", 'w')

data = []
rows = []

for n in range(0, len(TimestampStartCharges)):
    print(n)
    rows.append(TimestampStartCharges[n])
    rows.append(DaysSinceLastCharge[n])
    rows.append(StartingSoCs[n])
    rows.append(EndingSoCs[n])
    rows.append(DistanceTillCharge[n])

    data.append(rows)
    rows  = []

with newFile:
    writer = csv.writer(newFile)
    writer.writerows(data)